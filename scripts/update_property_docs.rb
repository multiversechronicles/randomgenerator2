#!/usr/bin/env ruby

require 'yaml'

entries = Dir.glob("../src/lib/metadata/*.yml", base: __dir__).map { |file| YAML.load_file(file) }

entries.sort! { |first, second|
    weight = first['weight'] <=> second['weight']
    if weight == 0
        first['name'] <=> second['name']
    else
        weight
    end
}

output = ""
category = ""

entries.each { |entry|
    if entry['category'] != category
        category = entry['category']
        output += "# #{category}\n\n"
    end

    output += "## #{entry['name']} (#{entry['id']})\n\n#{entry['description']}\n"
}

puts output
