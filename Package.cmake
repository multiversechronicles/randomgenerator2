
file(INSTALL /opt/win64/bin/ DESTINATION ${CMAKE_INSTALL_PREFIX} FILES_MATCHING PATTERN "*.dll")
file(INSTALL /opt/win64/plugins DESTINATION ${CMAKE_INSTALL_PREFIX})
file(INSTALL /opt/win64/qml DESTINATION ${CMAKE_INSTALL_PREFIX})
file(INSTALL /opt/win64/lib/grantlee DESTINATION ${CMAKE_INSTALL_PREFIX})

file(INSTALL /usr/x86_64-w64-mingw32/lib/libwinpthread-1.dll DESTINATION ${CMAKE_INSTALL_PREFIX})
file(INSTALL /usr/lib/gcc/x86_64-w64-mingw32/10-posix/libatomic-1.dll DESTINATION ${CMAKE_INSTALL_PREFIX})
file(INSTALL /usr/lib/gcc/x86_64-w64-mingw32/10-posix/libgcc_s_seh-1.dll DESTINATION ${CMAKE_INSTALL_PREFIX})
file(INSTALL /usr/lib/gcc/x86_64-w64-mingw32/10-posix/libstdc++-6.dll DESTINATION ${CMAKE_INSTALL_PREFIX})

