// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
// 
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include <memory>

#include <QObject>
#include <QUrl>

#include "fortuna_export.h"

namespace Fortuna {

class GeneratedObject;

class FORTUNA_EXPORT Generator : public QObject
{
    Q_OBJECT

public:
    Generator(QObject* parent = nullptr);
    ~Generator() override;

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(QUrl file READ file WRITE setFile NOTIFY fileChanged)
    QUrl file() const;
    void setFile(const QUrl& newFileName);
    Q_SIGNAL void fileChanged();

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(Fortuna::GeneratedObject* object READ object NOTIFY objectChanged)
    GeneratedObject* object() const;
    Q_SIGNAL void objectChanged();

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(QString propertyName READ propertyName WRITE setPropertyName NOTIFY propertyNameChanged)
    QString propertyName() const;
    void setPropertyName(const QString & newPropertyName);
    Q_SIGNAL void propertyNameChanged();

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(QString generatedText READ generatedText NOTIFY generatedTextChanged)
    QString generatedText() const;
    Q_SIGNAL void generatedTextChanged();

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(bool hasExceptions READ hasExceptions NOTIFY exceptionTextChanged)
    bool hasExceptions() const;

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(QString exceptionText READ exceptionText NOTIFY exceptionTextChanged)
    QString exceptionText() const;
    Q_SIGNAL void exceptionTextChanged();

    Q_INVOKABLE void generate();


    Q_INVOKABLE QString propertyText();

private:
    void setText(const QString& text);
    void setExceptionText(const QString& text);

    class Private;
    const std::unique_ptr<Private> d;
};

}
