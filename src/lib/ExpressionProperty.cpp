// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "ExpressionProperty.h"

#include <QJSEngine>
#include <QDebug>

#include "GeneratedObject.h"
#include "Exceptions.h"
#include "RandomEngine.h"

using namespace std::string_literals;
using namespace Fortuna;

FORTUNA_REGISTER_PROPERTY_TYPE(expression, ExpressionProperty::createExpressionProperty)

class ExpressionProperty::Private
{
public:
    QString expression;
    QVector<Property*> parameters;

    inline static std::shared_ptr<QJSEngine> engine;

    inline static const QStringList keywords = {
        "return"_qs,
        "if"_qs,
        "for"_qs,
        "do"_qs,
        "while"_qs,
        "switch"_qs,
        "var"_qs,
        "let"_qs,
        "const"_qs,
        "print"_qs,
    };
};

ExpressionProperty::ExpressionProperty(QObject* parent)
    : Property(parent), d(std::make_unique<Private>())
{
    if (!Private::engine) {
        Private::engine = std::make_shared<QJSEngine>();
        Private::engine->installExtensions(QJSEngine::AllExtensions);

        auto engine = RandomEngine::instance();
        Private::engine->globalObject().setProperty("Random"_qs, Private::engine->newQObject(engine.get()));

        auto clamp = Private::engine->evaluate("(function(value, min, max) { return Math.min(Math.max(min, value), max) })"_qs);
        Private::engine->globalObject().property("Math"_qs).setProperty("clamp"_qs, clamp);
        Private::engine->globalObject().setProperty("clamp"_qs, clamp);
        auto fallback = Private::engine->evaluate("(function(values) { for (let value of values) { if (value) return value; } })"_qs);
        Private::engine->globalObject().setProperty("fallback"_qs, fallback);

        auto merge = Private::engine->evaluate("(function(values) { let result = {}; for (let value of values) { result = Object.assign({}, result, value); } return result })"_qs);
        Private::engine->globalObject().setProperty("merge"_qs, merge);
    }
}

ExpressionProperty::~ExpressionProperty()
{
}

void ExpressionProperty::generate()
{
    Property::generate();

    auto scriptString = "(function(%1) {\n%2\n})"_qs;

    auto params = parameters();
    auto args = QStringList{};
    std::transform(params.cbegin(), params.cend(), std::back_inserter(args), [](auto param) {
        return param->name();
    });
    args.prepend("tags"_qs);
    args.prepend("parent"_qs);
    args.prepend("self"_qs);

    auto expression = parameterValue<QString>("expression"_qs).value_or(QString{});

    scriptString = scriptString.arg(args.join(", "_qs));
    if (!expression.contains(QLatin1Char('\n'))) {
        scriptString = scriptString.arg("return %1"_qs.arg(expression));
    } else {
        scriptString = scriptString.arg(expression);
    }

    auto result = Private::engine->evaluate(scriptString, name());
    if (result.isError()) {
        auto message = result.toString();
        message += " at line %1"_qs.arg(result.property("lineNumber"_qs).toInt() - 1);
        throw ExpressionException(path(), message, expression);
    }

    auto self = findSelf();
    if (!self) {
        throw PropertyException(path(), "Could not find 'self' object"_qs);
    }

    auto valueList = QJSValueList{};

    valueList.append(Private::engine->toScriptValue(self));

    auto parentProperty = qobject_cast<Property*>(parent());
    if (parentProperty) {
        valueList.append(Private::engine->newQObject(parentProperty));
    } else {
        valueList.append(QJSValue());
    }

    auto tagsObject = QQmlPropertyMap{};
    QVariantMap tags;
    findTags(findFileRoot(), tags);
    for (auto itr = tags.constKeyValueBegin(); itr != tags.constKeyValueEnd(); ++itr) {
        tagsObject.insert(itr->first, itr->second);
    }
    valueList.append(Private::engine->newQObject(&tagsObject));

    std::transform(params.cbegin(), params.cend(), std::back_inserter(valueList), [](auto param) {
        return Private::engine->toScriptValue(param->value());
    });

    auto value = result.call(valueList);
    if (value.isError()) {
        auto message = value.toString();
        message += " at line %1"_qs.arg(value.property("lineNumber"_qs).toInt() - 1);
        throw ExpressionException(path(), message, expression);
    }

    setValue(value.toVariant());

    result = QJSValue{};
    d->engine->collectGarbage();
}

QString ExpressionProperty::expression() const
{
    return d->expression;
}

Property* ExpressionProperty::createExpressionProperty(const YAML::Node& yaml)
{
    auto property = new ExpressionProperty();

    property->addRequiredParameter("expression"_qs);
    property->addParameters(yaml);

    return property;
}

Property* ExpressionProperty::findFileRoot()
{
    Property* object = this;
    while (object->parent()) {
        if (object->parent()->inherits("Fortuna::IncludeProperty")) {
            break;
        }
        object = qobject_cast<Property*>(object->parent());
    }

    return object;
}
