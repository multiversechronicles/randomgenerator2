// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include "Property.h"

namespace Fortuna {

class RandomValueProperty : public Property
{
    Q_OBJECT
    Q_PROPERTY(Fortuna::Property* from READ from CONSTANT)
    Q_PROPERTY(Fortuna::Property* to READ to CONSTANT)

public:
    enum class Type {
        Integer,
        Float
    };

    RandomValueProperty(QObject* parent = nullptr);
    ~RandomValueProperty() override;

    void generate() override;

    Property* from() const;
    Property* to() const;

    static Property* createRandomIntegerProperty(const YAML::Node& yaml);
    static Property* createRandomFloatProperty(const YAML::Node& yaml);

private:
    Type m_type;
};

}
