// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "ListProperty.h"

#include <QDebug>

#include "GeneratedObject.h"

using namespace std::string_literals;
using namespace Fortuna;

FORTUNA_REGISTER_PROPERTY_TYPE(list, ListProperty::createListProperty)

ListProperty::ListProperty(QObject* parent)
    : Property(parent)
{
    setDefaultValue(QVariantList{});
}

ListProperty::~ListProperty()
{
}

int ListProperty::count() const
{
    return childProperties().count();
}

void ListProperty::generate()
{
    Property::generate();

    auto count = parameterValue<int>("count"_qs).value_or(0);
    if (count == 0) {
        return;
    }

    for (int i = 0; i < count; ++i) {
        auto property = generateProperty("%1_%2"_qs.arg(name()).arg(i).toStdString(), m_template, this, {
            {"list_index"_qs, YAML::Node(i)}
        });
        addChildProperty(property);
    }

    setValue(childPropertyValues());
}

QString ListProperty::toString(int indentation) const
{
    QString result = indent(indentation) + name() + ":\n"_qs;

    const auto children = childProperties();
    for (auto entry : children) {
        result += indent(indentation) + "  - "_qs + entry->toString(indentation + 1).trimmed() + "\n"_qs;
    }

    return result;
}

Property* ListProperty::createListProperty(const YAML::Node& yaml)
{
    auto property = new ListProperty();

    if (!yaml["list"]) {
        throw GeneratorException("'list' is a required parameter"s);
    }

    property->m_template = yaml["list"];

    property->addRequiredParameter("count"_qs);
    property->addParameters(yaml, {"list"_qs});

    return property;
}
