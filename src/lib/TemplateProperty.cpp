// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "TemplateProperty.h"

#include <grantlee/engine.h>

#include <QDebug>

#include "GeneratedObject.h"

using namespace std::string_literals;
using namespace Fortuna;

FORTUNA_REGISTER_PROPERTY_TYPE(template, TemplateProperty::createTemplateProperty)

class TemplateProperty::Private
{
public:
    QString templateString;
    QVector<Property*> parameters;

    inline static std::shared_ptr<Grantlee::Engine> engine;
};

TemplateProperty::TemplateProperty(QObject* parent)
    : Property(parent), d(std::make_unique<Private>())
{
    if (!Private::engine) {
        Private::engine = std::make_shared<Grantlee::Engine>();
        Private::engine->setSmartTrimEnabled(true);
    }
}

TemplateProperty::~TemplateProperty()
{
}

void TemplateProperty::generate()
{
    Property::generate();

    auto templateString = parameterValue<QString>("template"_qs).value_or(QString{});

    auto templateObject = Private::engine->newTemplate(templateString, name());
    if (templateObject->error() != Grantlee::NoError) {
        throw PropertyException(path(), "Failed to parse template: "_qs + templateObject->errorString());
    }

    QVariantHash mapping;

    auto self = findSelf();
    if (self) {
        mapping.insert("self"_qs, QVariant::fromValue(self));
    }

    const auto params = parameters();
    for (auto parameter : params) {
        if (parameter->name() == "template"_qs) {
            continue;
        }
        mapping.insert(parameter->name(), parameter->value());
    }

    auto context = Grantlee::Context(mapping);
    setValue(templateObject->render(&context));

    if (templateObject->error() != Grantlee::NoError) {
        throw PropertyException(path(), "Failed to process template: "_qs + templateObject->errorString());
    }
}

QString TemplateProperty::templateString() const
{
    return parameterValue<QString>("template"_qs).value_or(QString{});
}

QString TemplateProperty::toString(int indentation) const
{
    auto valueString = value().toString();
    if (valueString.indexOf(QLatin1Char('\n')) == -1) {
        return indent(indentation) + name() + ": \""_qs + valueString + "\"\n"_qs;
    }

    QString result = indent(indentation) + name() + ": |\n"_qs;

    const auto parts = value().toString().split(QLatin1Char('\n'));
    for (auto line : parts) {
        result += indent(indentation + 1) + line + "\n"_qs;
    }

    return result;
}

Property * TemplateProperty::createTemplateProperty(const YAML::Node& yaml)
{
    auto property = new TemplateProperty();

    property->addRequiredParameter("template"_qs);
    property->addParameters(yaml);

    return property;
}
