// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "RandomValueProperty.h"

#include <random>

#include <QDebug>

#include "RandomEngine.h"

using namespace std::string_literals;
using namespace Fortuna;

FORTUNA_REGISTER_PROPERTY_TYPE(random_integer, RandomValueProperty::createRandomIntegerProperty);
FORTUNA_REGISTER_PROPERTY_TYPE(random_float, RandomValueProperty::createRandomFloatProperty);

RandomValueProperty::RandomValueProperty(QObject* parent)
    : Property(parent)
{
}

RandomValueProperty::~RandomValueProperty()
{
}

void RandomValueProperty::generate()
{
    Property::generate();

    if (m_type == Type::Integer) {
        auto from = parameterValue<int>("from"_qs).value_or(0);
        auto to = parameterValue<int>("to"_qs).value_or(0);
        auto modifier = parameterValue<int>("modifier"_qs).value_or(0);

        setValue(RandomEngine::instance()->intValue(from, to) + modifier);
    } else {
        auto from = parameterValue<float>("from"_qs).value_or(0.0);
        auto to = parameterValue<float>("to"_qs).value_or(0.0);
        auto modifier = parameterValue<float>("modifier"_qs).value_or(0.0);

        setValue(RandomEngine::instance()->realValue(from, to) + modifier);
    }
}

Property* RandomValueProperty::from() const
{
    return parameter("from"_qs);
}

Property* RandomValueProperty::to() const
{
    return parameter("to"_qs);
}

Property* RandomValueProperty::createRandomIntegerProperty(const YAML::Node& yaml)
{
    auto property = new RandomValueProperty();
    property->m_type = Type::Integer;

    property->addRequiredParameter("from"_qs);
    property->addRequiredParameter("to"_qs);
    property->addParameters(yaml);

    return property;
}

Property * RandomValueProperty::createRandomFloatProperty(const YAML::Node& yaml)
{
    auto property = new RandomValueProperty();
    property->m_type = Type::Float;

    property->addRequiredParameter("from"_qs);
    property->addRequiredParameter("to"_qs);
    property->addParameters(yaml);

    return property;
}
