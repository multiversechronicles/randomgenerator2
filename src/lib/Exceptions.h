// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include <exception>
#include <sstream>
#include <QString>

namespace Fortuna {

class GeneratorException : public std::exception
{
public:
    GeneratorException() { }
    GeneratorException(const std::string& message) { m_message = message; }
    GeneratorException(const QString& message) { m_message = message.toStdString(); }
    inline const char* what() const noexcept override { return m_message.data(); }

protected:
    std::string m_message;
};

class PropertyException : public GeneratorException
{
public:
    PropertyException(const QString& path, const QString& message)
    {
        std::stringstream stream;
        stream << "Exception in property " << path.toStdString() << ":\n";
        stream << "    " << message.toStdString() << "\n";
        m_message = stream.str();
    }
};

class ExpressionException : public GeneratorException
{
public:
    ExpressionException(const QString& path, const QString& message, const QString& script)
    {
        std::stringstream stream;
        stream << "Exception in expression for " << path.toStdString() << ":\n";
        stream << "    " << message.toStdString() << "\n";
        stream << "    Expression:\n";

        const auto lines = script.split(QLatin1Char('\n'));
        for (auto line : lines) {
            stream << "        " << line.toStdString() << "\n";
        }

        m_message = stream.str();
    }
};

}
