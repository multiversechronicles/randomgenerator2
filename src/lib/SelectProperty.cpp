// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "SelectProperty.h"

#include <QDebug>

using namespace Fortuna;

FORTUNA_REGISTER_PROPERTY_TYPE(select, SelectProperty::createSelectProperty)

SelectProperty::SelectProperty(QObject* parent)
    : Property(parent)
{
}

SelectProperty::~SelectProperty()
{
}

void SelectProperty::generate()
{
    Property::generate();

    auto selected = parameterValue<QString>("select"_qs).value_or(QString{});
    if (selected.isEmpty()) {
        return;
    }

    auto matchCase = parameterValue<bool>("match_case"_qs).value_or(true);

    const auto params = parameters();
    for (auto param : params) {
        if (param->name().compare(selected, matchCase ? Qt::CaseSensitive : Qt::CaseInsensitive) == 0) {
            setValue(param->value());
            break;
        }
    }
}

Property* SelectProperty::createSelectProperty(const YAML::Node& yaml)
{
    auto property = new SelectProperty();

    property->addRequiredParameter("select"_qs);
    property->addParameters(yaml);

    return property;
}
