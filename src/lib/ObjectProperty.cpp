// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "ObjectProperty.h"

#include <QDebug>

#include "GeneratedObject.h"

using namespace std::string_literals;
using namespace Fortuna;

FORTUNA_REGISTER_PROPERTY_TYPE(object, ObjectProperty::createObjectProperty)

ObjectProperty::ObjectProperty(QObject* parent)
    : Property(parent)
{
}

ObjectProperty::~ObjectProperty()
{
}

GeneratedObject* ObjectProperty::object() const
{
    return m_object;
}

void ObjectProperty::generate()
{
    Property::generate();

    auto self = findSelf();
    auto fileName = parameterValue<QString>("object"_qs).value_or(QString{});
    if (fileName.isEmpty()) {
        return;
    }

    auto filePath = self->filePath().replace_filename(fileName.toStdString());

    GeneratedObject* object = nullptr;
    try {
        object = GeneratedObject::fromFile(filePath);
    } catch (const GeneratorException& e) {
        throw PropertyException(path(), "Failed to load object: "_qs + QString::fromUtf8(e.what()));
    }

    if (!object) {
        throw PropertyException(path(), "Specified file does not exist"_qs);
    }
    object->setParent(this);

    const auto params = parameters();
    for (auto param : params) {
        if (param->name() == "object"_qs) {
            continue;
        }

        object->setPropertyValue(param->name(), param->value());
    }

    object->generate();
    addExceptions(object->exceptions());

    m_object = object;

    setValue(QVariant::fromValue(m_object));
}

QString ObjectProperty::toString(int indentation) const
{
    if (m_object) {
        return m_object->toString(indentation);
    }

    return indent(indentation) + "(Empty)\n"_qs;
}

Property* ObjectProperty::createObjectProperty(const YAML::Node& yaml)
{
    auto property = new ObjectProperty();

    property->addRequiredParameter("object"_qs);
    property->addParameters(yaml);

    return property;
}
