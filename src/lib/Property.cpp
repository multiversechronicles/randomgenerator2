#include "Property.h"

#include <unordered_map>
#include <iostream>
#include <mutex>

#include <QDebug>
#include <QFile>

#include "GeneratedObject.h"
#include "Exceptions.h"

using namespace std::string_literals;

using namespace Fortuna;

void initResources()
{
    Q_INIT_RESOURCE(metadata);
}

FORTUNA_REGISTER_PROPERTY_TYPE(literal, Property::createLiteral)

struct PropertyMetadata
{
    std::string id;
    QString name;
    QString description;
    QString category;
    int weight;
};

class FORTUNA_NO_EXPORT Property::Private
{
public:
    QString name;
    QVariant value;
    bool hasValue = false;
    QString tag;

    QSet<QString> requiredParameters;
    QVector<Property*> parameters;
    QQmlPropertyMap* parameterMap = nullptr;

    QVector<GeneratorException> exceptions;

    QVector<Property*> childProperties;

    static std::unordered_map<std::string, FactoryFunction>& factories();
    static std::unordered_map<std::string, QVariantMap>& metadata();
};

Property::Property(QObject* parent)
    : QObject(parent), d(std::make_unique<Private>())
{
    d->parameterMap = new QQmlPropertyMap(this);
}

Property::~Property()
{
}

QString Property::name() const
{
    return d->name;
}

QString Property::path() const
{
    auto path = d->name;

    auto p = parent();
    while (p) {
        path.prepend(QLatin1Char('/'));
        path.prepend(p->objectName());
        p = p->parent();
    }

    return path;
}

QVariant Property::value() const
{
    return d->value;
}

bool Property::hasValue() const
{
    return d->hasValue;
}

void Property::setValue(const QVariant& value)
{
    d->hasValue = true;
    d->value = value;
}

void Property::generate()
{
    QVector<GeneratorException> exceptions;

    for (auto entry : std::as_const(d->parameters)) {
        if (!entry->hasValue()) {
            try {
                entry->generate();
                d->parameterMap->insert(entry->name(), entry->value());
            } catch (const GeneratorException& e) {
                exceptions.push_back(e);
            }
            exceptions.append(entry->exceptions());
        } else {
            d->parameterMap->insert(entry->name(), entry->value());
        }
    }

    d->exceptions = exceptions;
}

void Property::reset()
{
    d->value = QVariant{};
}

QString Property::type() const
{
    return QString::fromUtf8(metaObject()->className());
}

QString Property::tag() const
{
    return d->tag;
}

QVector<Property*> Property::parameters() const
{
    return d->parameters;
}

QQmlPropertyMap* Property::parameterPropertyMap() const
{
    return d->parameterMap;
}

Property* Property::parameter(const QString& name) const
{
    auto itr = std::find_if(d->parameters.cbegin(), d->parameters.cend(), [name](auto entry) {
        return entry->name() == name;
    });

    if (itr != d->parameters.cend()) {
        return *itr;
    }

    return nullptr;
}

void Property::addParameter(const QString& name, Property* parameter)
{
    parameter->setParent(this);
    d->parameters << parameter;
    d->parameterMap->insert(name, parameter->value());
}

void Property::addRequiredParameter(const QString& name)
{
    d->requiredParameters.insert(name);
}

void Property::addParameters(const YAML::Node& yaml, const QSet<QString>& exclude)
{
    if (!yaml) {
        return;
    }

    for (auto itr = yaml.begin(); itr != yaml.end(); ++itr) {
        auto key = itr->first.as<QString>();

        if (key.isEmpty()) {
            continue;
        }

        if (exclude.contains(key)) {
            continue;
        }

        if (key.startsWith(QLatin1Char('.'))) {
            continue;
        }

        auto property = Property::fromYAML(key.toStdString(), itr->second);
        if (property) {
            addParameter(key, property);
        }
    }

    for (auto required : std::as_const(d->requiredParameters)) {
        if (!d->parameterMap->contains(required)) {
            throw GeneratorException(required + " is a required parameter"_qs);
        }
    }
}

void Property::removeParameter(const QString& name)
{
    auto itr = std::find_if(d->parameters.begin(), d->parameters.end(), [name](auto entry) {
        return entry->name() == name;
    });

    if (itr != d->parameters.end()) {
        d->parameters.erase(itr);
    }
}

QVector<Fortuna::Property*> Fortuna::Property::childProperties() const
{
    return d->childProperties;
}

QVariantList Fortuna::Property::childPropertyValues() const
{
    QVariantList value;
    std::transform(d->childProperties.cbegin(), d->childProperties.cend(), std::back_inserter(value), [](Property* entry) {
        return entry->value();
    });
    return value;
}

int Fortuna::Property::childPropertyCount() const
{
    return d->childProperties.count();
}

void Fortuna::Property::addChildProperty(Fortuna::Property* child)
{
    child->setParent(this);
    d->childProperties.append(child);
    addExceptions(child->exceptions());
}

void Fortuna::Property::removeChildProperty(Fortuna::Property* child)
{
    child->setParent(nullptr);
    d->childProperties.removeOne(child);
}

QVector<GeneratorException> Property::exceptions() const
{
    return d->exceptions;
}

QString Property::toString(int indentation) const
{
    auto quotedString = [](const QVariant& input, int indentation) {
        if (!input.isValid()) {
            return QString{};
        }

        if (input.type() == QVariant::String) {
            auto valueString = input.toString();
            if (!valueString.contains(QLatin1Char('\n'))) {
                return "\"%1\"\n"_qs.arg(input.toString());
            }

            QString result = "|\n"_qs;
            const auto parts = valueString.split(QLatin1Char('\n'));
            for (auto line : parts) {
                result += Property::indent(indentation + 1) + line + QLatin1Char('\n');
            }

            return result;
        } else if (input.canConvert<GeneratedObject*>()) {
            auto object = input.value<GeneratedObject*>();
            if (object) {
                return QString{object->toString(indentation).trimmed() + QLatin1Char('\n')};
            }
        }

        return QString{input.toString() + "\n"_qs};
    };

    QString valueString;
    switch (d->value.type()) {
        case QVariant::List: {
            valueString += indent(indentation) + d->name + ":\n"_qs;

            const auto list = d->value.toList();
            for (const auto& entry : list) {
                valueString += indent(indentation + 1) + "- "_qs + quotedString(entry, indentation + 1);
            }

            break;
        }
        case QVariant::Map: {
            valueString += indent(indentation) + d->name + ":\n"_qs;

            const auto map = d->value.toMap();
            for (auto itr = map.keyValueBegin(); itr != map.keyValueEnd(); ++itr) {
                valueString += indent(indentation + 1) + itr->first + ": "_qs + quotedString(itr->second, indentation + 1);
            }

            break;
        }
        default: {
            valueString = quotedString(d->value, indentation);

            if (valueString.isEmpty()) {
                valueString += "("_qs;
                valueString += d->value.isValid() ? QString::fromUtf8(d->value.typeName()) : "Empty"_qs;
                valueString += ")\n"_qs;
            }

            valueString = indent(indentation) + d->name + ": "_qs + valueString;
        }
    }

    return valueString;
}

Property* Property::fromYAML(const std::string& name, const YAML::Node& yaml)
{
    Q_ASSERT(!name.empty());

    auto factories = Private::factories();
    std::function<Property*(const YAML::Node& yaml)> factory;
    if (yaml.IsMap()) {
        if (yaml["type"]) {
            auto itr = factories.find(yaml["type"].Scalar());
            if (itr != factories.end()) {
                factory = (*itr).second;
            } else {
                throw GeneratorException{"Unknown type "_qs + QString::fromStdString(yaml["type"].Scalar()) + " for property "_qs + QString::fromStdString(name)};
            }
        } else {
            std::vector<decltype(factories)::value_type> matches;
            std::copy_if(factories.cbegin(), factories.cend(), std::back_inserter(matches), [yaml](const auto& entry) {
                return yaml[entry.first].IsDefined();
            });

            if (matches.size() == 1) {
                auto itr = std::find_if(factories.begin(), factories.end(), [yaml](const auto& entry) {
                    return yaml[entry.first].IsDefined();
                });
                factory = (*itr).second;
            } else if (matches.size() > 1) {
                QStringList types;
                std::transform(matches.cbegin(), matches.cend(), std::back_inserter(types), [](const auto& entry) {
                    return QString::fromStdString(entry.first);
                });
                throw GeneratorException{"Ambiguous property type for property "_qs + QString::fromStdString(name) + ", candidates: "_qs + types.join(", "_qs)};
            }
        }
    }

    if (!factory) {
        factory = factories.at("literal"s);
    }

    Property* property = nullptr;
    property = factory(yaml);

    property->d->name = QString::fromStdString(name);
    property->setObjectName(QString::fromStdString(name));

    property->setTag(yaml.Tag());

    return property;
}

void Property::registerPropertyType(const std::string& type, const std::string& metadataPath, const FactoryFunction& factory)
{
    auto& factories = Private::factories();
    auto& metadata = Private::metadata();

    auto itr = std::find_if(factories.cbegin(), factories.cend(), [type](const auto& entry) {
        return entry.first == type;
    });

    if (itr != factories.cend()) {
        qWarning() << "Attempting to re-register Property type!" << type.data();
        return;
    }

    factories.insert(std::make_pair(type, factory));

    static std::once_flag flag;
    std::call_once(flag, initResources);

    auto path = QString::fromStdString(metadataPath);
    QFile metadataFile(path);
    if (!metadataFile.open(QIODevice::ReadOnly)) {
        qWarning() << "Could not open metadata file" << path << "for property type" << type.data();
        metadata.insert(std::make_pair(type, QVariantMap{}));
        return;
    }

    auto contents = metadataFile.readAll();
    YAML::Node node;
    try {
        node = YAML::Load(contents.data());
    } catch (YAML::Exception& e) {
        qWarning() << "An exception occurred parsing metadata file" << path << e.what();
    }

    if (!node.IsMap()) {
        qWarning() << "Metadata for property type" << type.data() << "is not a map, ignoring.";
        metadata.insert(std::make_pair(type, QVariantMap{}));
    } else {
        metadata.insert(std::make_pair(type, node.as<QVariantMap>()));
    }
}

QString Property::listTypes()
{
    std::stringstream result;

    auto& factories = Private::factories();
    auto& metadata = Private::metadata();

    std::vector<PropertyMetadata> entries;

    for (auto itr = factories.cbegin(); itr != factories.cend(); ++itr) {
        auto propertyId = (*itr).first;
        auto propertyMetadata = metadata.at((*itr).first);

        auto& entry = entries.emplace_back();
        entry.id = propertyId;
        entry.name = propertyMetadata.value("name"_qs, QString::fromStdString(propertyId)).toString();
        entry.description = propertyMetadata.value("description"_qs, "No description available.\n"_qs).toString();
        entry.category = propertyMetadata.value("category"_qs, "Unknown"_qs).toString();
        entry.weight = propertyMetadata.value("weight"_qs, 100).toInt();
    }

    std::sort(entries.begin(), entries.end(), [](const auto& first, const auto& second) {
        if (first.weight == second.weight) {
            return first.name < second.name;
        }
        return first.weight < second.weight;
    });

    QString category;
    for (auto entry : entries) {
        if (category != entry.category) {
            category = entry.category;
            result << "# " << qPrintable(category) << "\n\n";
        }

        result << "## " << qPrintable(entry.name) << " (" << entry.id << ")\n\n";
        result << qPrintable(entry.description) << "\n";
    }

    result.flush();

    return QString::fromStdString(result.str());
}

QVariantMap Property::metadataForType(const std::string& type)
{
    auto metadata = Private::metadata();
    if (auto itr = metadata.find(type); itr != metadata.end()) {
        return itr->second;
    }
    return QVariantMap{};
}

Property* Property::createLiteral(const YAML::Node& yaml)
{
    auto property = new Property();
    property->d->value = yaml.as<QVariant>();
    return property;
}

GeneratedObject* Property::findSelf()
{
    GeneratedObject* selfObject = nullptr;
    QObject* currentObject = this;
    while (!selfObject && currentObject) {
        selfObject = qobject_cast<GeneratedObject*>(currentObject->parent());
        currentObject = currentObject->parent();
    }
    return selfObject;
}

QString Property::indent(int indentation)
{
    return QString{indentation * 4, QLatin1Char(' ')};
}

void Property::setDefaultValue(const QVariant& defaultValue)
{
    d->value = defaultValue;
}

void Property::addExceptions(const QVector<GeneratorException> &exceptions)
{
    d->exceptions.append(exceptions);
}

QVariant Property::parameterVariant(const QString& name) const
{
    auto param = parameter(name);
    if (!param) {
        return QVariant{};
    }

    return param->value();
}

Property* Property::generateProperty(const std::string& name, const YAML::Node& yaml, Fortuna::Property* parent)
{
    return generateProperty(name, yaml, parent, QMap<QString, Property*>{});
}

Property* Property::generateProperty(const std::string& name, const YAML::Node& yaml, Fortuna::Property* parent, const QMap<QString, YAML::Node>& extraParameters)
{
    QMap<QString, Property*> parameters;
    for (auto itr = extraParameters.keyValueBegin(); itr != extraParameters.keyValueEnd(); ++itr) {
        auto parameter = Property::fromYAML(itr->first.toStdString(), itr->second);
        if (parameter) {
            parameters.insert(itr->first, parameter);
        }
    }

    return generateProperty(name, yaml, parent, parameters);
}

Property* Property::generateProperty(const std::string& name, const YAML::Node& yaml, Fortuna::Property* parent, const QMap<QString, Fortuna::Property *>& extraParameters)
{
    auto property = fromYAML(name, yaml);
    property->setParent(parent);

    for (auto itr = extraParameters.keyValueBegin(); itr != extraParameters.keyValueEnd(); ++itr) {
        property->addParameter(itr->first, itr->second);
    }

    try {
        property->generate();
    } catch (GeneratorException& e) {
        property->addExceptions({e});
    }

    return property;
}

void Property::setTag(const std::string& tag)
{
    if (tag.empty()) {
        return;
    }

    if (tag == "!"s || tag == "?"s) {
        return;
    }

    d->tag = QString::fromStdString(tag).mid(1);
}

void Property::findTags(Property* root, QVariantMap& output)
{
    if (!root->tag().isEmpty()) {
        if (output.contains(root->tag())) {
            return;
        }

        output.insert(root->tag(), root->value());
    }

    const auto c = root->children();
    for (auto& child : c) {
        auto prop = qobject_cast<Property*>(child);
        if (!prop) {
            continue;
        }

        findTags(prop, output);
    }
}

std::unordered_map<std::string, std::function<Property*(const YAML::Node &)>> &
Property::Private::factories()
{
    static std::unordered_map<std::string, std::function<Property*(const YAML::Node&)>> factory_map;
    return factory_map;
}

std::unordered_map<std::string, QVariantMap>& Property::Private::metadata()
{
    static std::unordered_map<std::string, QVariantMap> metadata_map;
    return metadata_map;
}
