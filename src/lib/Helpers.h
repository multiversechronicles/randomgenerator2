// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include <QString>

inline QString operator"" _qs(const char* characters, size_t length)
{
    return QString::fromUtf8(characters, length);
}
