// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include <memory>
#include <functional>
#include <optional>

#include <QObject>
#include <QQmlPropertyMap>
#include <QSet>

#include <yaml-cpp/yaml.h>

#include "YamlQtSupport.h"
#include "Helpers.h"
#include "Exceptions.h"

#include "fortuna_export.h"

namespace Fortuna {

class GeneratedObject;
class Property;
class GeneratorException;

class FORTUNA_EXPORT Property : public QObject
{
    Q_OBJECT

public:
    enum ParameterOptions
    {
        NoOptions = 0,
        RequiredParameter = 1
    };
    Q_FLAGS(ParameterOptions)

    using FactoryFunction = std::function<Property*(const YAML::Node&)>;

    Property(QObject *parent = nullptr);
    ~Property() override;

    Q_PROPERTY(QObject* parent READ parent CONSTANT)

    Q_PROPERTY(QString name READ name CONSTANT)
    virtual QString name() const;
    QString path() const;

    Q_PROPERTY(QVariant value READ value CONSTANT)
    virtual QVariant value() const;
    void setValue(const QVariant& value);
    virtual bool hasValue() const;

    Q_PROPERTY(QString type READ type CONSTANT)
    QString type() const;

    Q_PROPERTY(QString tag READ tag CONSTANT)
    QString tag() const;

    Q_PROPERTY(QQmlPropertyMap* parameters READ parameterPropertyMap CONSTANT)
    QVector<Property*> parameters() const;
    QQmlPropertyMap* parameterPropertyMap() const;
    Q_INVOKABLE Fortuna::Property* parameter(const QString& name) const;

    template <typename T>
    inline std::optional<T> parameterValue(const QString& name) const {
        auto variant = parameterVariant(name);
        if (variant.isNull()) {
            return std::optional<T>();
        } else {
            return std::optional<T>(variant.value<T>());
        }
    }

    void addParameter(const QString& name, Property* parameter);
    void addParameters(const YAML::Node& yaml, const QSet<QString>& exclude = {});
    void addRequiredParameter(const QString& name);
    void removeParameter(const QString& name);

    QVector<Property*> childProperties() const;
    QVariantList childPropertyValues() const;
    int childPropertyCount() const;
    void addChildProperty(Property* child);
    void removeChildProperty(Property* child);

    virtual void generate();
    virtual void reset();

    QVector<GeneratorException> exceptions() const;

    virtual QString toString(int indentation = 0) const;

    static Property* fromYAML(const std::string& name, const YAML::Node& yaml);

    static void registerPropertyType(const std::string& type, const std::string& metadataPath, const FactoryFunction& factory);
    static QString listTypes();
    static QVariantMap metadataForType(const std::string& type);

    static Property* createLiteral(const YAML::Node& yaml);

    static QString indent(int indentation);

protected:
    GeneratedObject* findSelf();
    void setDefaultValue(const QVariant& defaultValue);
    void addExceptions(const QVector<GeneratorException>& exceptions);
    QVariant parameterVariant(const QString& name) const;
    Property* generateProperty(const std::string& name, const YAML::Node& yaml, Property* parent);
    Property* generateProperty(const std::string& name, const YAML::Node& yaml, Property* parent, const QMap<QString, YAML::Node>& extraParameters);
    Property* generateProperty(const std::string& name, const YAML::Node& yaml, Property* parent, const QMap<QString, Property*>& extraParameters);

    void setTag(const std::string& tag);
    virtual void findTags(Property* root, QVariantMap& output);

private:
    class Private;
    const std::unique_ptr<Private> d;
};

}

#define FORTUNA_REGISTER_PROPERTY_TYPE(type, factory) \
    FORTUNA_REGISTER_PROPERTY_TYPE_WITH_METADATA_PATH(type, factory, \
        std::string(":/org.multiversechronicles.fortuna/metadata/" #type ".yml"))

#define FORTUNA_REGISTER_PROPERTY_TYPE_WITH_METADATA_PATH(type, factory, metadata) \
    namespace Fortuna { struct PropertyTypeRegistration##type { \
        PropertyTypeRegistration##type () { Property::registerPropertyType(std::string(#type), metadata, factory); } \
    }; \
    static PropertyTypeRegistration##type type##_registration; }
