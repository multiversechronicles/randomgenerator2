id: random_float
name: "Random Floating-Point Value"
weight: -4
category: Generation
description: |
    Generate a random floating point number between a start and end value,
    inclusive.

    ### Parameters
    * **from** (required): The start of the range.
    * **to** (required): The end of the range.
    * **modifier**: A fixed amount to add to the generated value.

    ### Examples

    Generate a random value between 5 and 10.

    ```yaml
    float_example:
        type: random_float
        from: 5
        to: 10
    ```

    Generate a random value between 0 and 10 that is the sum of two random
    values between 0 and 5.

    ```yaml
    float_example:
        type: random_float
        from: 0
        to: 5
        modifier:
            type: random_float
            from: 0
            to: 5
    ```

    ### Note

    Since this type lacks a parameter that matches its type name, you will need
    to explicitly indicate the type using `type: random_float`.
