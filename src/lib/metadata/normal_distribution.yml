id: normal_distribution
name: Normal Distribution
weight: -5
category: Generation
description: |
    Generates a floating-point value from a normal distribution, with the
    specified mean and standard deviation.

    ### Parameters

    * **mean** (required): The mean of the distribution.
    * **stddev** (required): The standard deviation of the distribution.
    * **min**: The minimum value that should be generated.
    * **max**: The maximum value that should be generated.

    ### Examples

    Generate a number between 0 and 5 from a normal distribution.

    ```yaml
    normal_example:
        type: normal_distribution
        mean: 2.5
        stddev: 1
        min: 0
        max: 5
    ```

    ### Note

    Since this type lacks a parameter that matches its type name, you will need
    to explicitly indicate the type using `type: normal_distribution`.
