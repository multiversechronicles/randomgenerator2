// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "IncludeProperty.h"

#include <random>

#include <QDebug>

#include "GeneratedObject.h"
#include "Exceptions.h"

using namespace std::string_literals;
using namespace Fortuna;

FORTUNA_REGISTER_PROPERTY_TYPE(include, IncludeProperty::createIncludeProperty)

IncludeProperty::IncludeProperty(QObject* parent)
    : Property(parent)
{
}

IncludeProperty::~IncludeProperty()
{
}

Property * IncludeProperty::includedProperty() const
{
    return m_includedProperty;
}

void IncludeProperty::generate()
{
    Property::generate();

    auto includeFile = parameterValue<QString>("include"_qs).value_or(QString{});

    if (includeFile.isEmpty()) {
        throw PropertyException(path(), "'include' parameter cannot be empty"_qs);
    }

    auto self = findSelf();
    auto filePath = self->filePath().replace_filename(includeFile.toStdString());

    YAML::Node yaml;
    try {
        yaml = YAML::LoadFile(filePath.string());
    } catch(YAML::BadFile& e) {
        throw PropertyException(path(), "Could not find file "_qs + QString::fromStdString(filePath.string()));
    } catch (YAML::ParserException& e) {
        throw PropertyException(path(), "Error parsing file %1: %2"_qs
                                    .arg(QString::fromStdString(filePath.string()))
                                    .arg(QString::fromUtf8(e.what())));
    } catch (YAML::Exception& e) {
        throw GeneratorException("YAML error in file %1: %2"_qs
                                    .arg(QString::fromStdString(filePath.string()))
                                    .arg(QString::fromUtf8(e.what())));
    }

    if (!yaml.IsMap()) {
        throw PropertyException(path(), "Included file "_qs + includeFile + "does not define a single property"_qs);
    }

    auto parentProperty = qobject_cast<Property*>(parent());
    while (parentProperty) {
        auto include = qobject_cast<IncludeProperty*>(parentProperty);
        parentProperty = qobject_cast<Property*>(parentProperty->parent());
        if (!include) {
            continue;
        }

        auto file = include->parameterValue<QString>("include"_qs).value_or(QString{});
        if (file == includeFile) {
            QString error = "Circular include detected: "_qs + path() + " includes "_qs + includeFile + " but is already being included by "_qs + include->path();
            throw PropertyException(path(), error);
        }
    }

    QString propertyName = "include-"_qs + QString::fromStdString(filePath.filename().string());
    m_includedProperty = Property::fromYAML(propertyName.toStdString(), yaml);
    m_includedProperty->setParent(this);

    const auto params = parameters();
    for (auto& param : params) {
        if (param->name() == "include"_qs) {
            continue;
        }

        if (auto p = m_includedProperty->parameter(param->name()); p) {
            p->setValue(param->value());
        }
    }

    try {
        m_includedProperty->generate();
    } catch (GeneratorException& e) {
        addExceptions({e});
    }

    addChildProperty(m_includedProperty);
    setValue(m_includedProperty->value());
}

Property* IncludeProperty::createIncludeProperty(const YAML::Node& yaml)
{
    auto property = new IncludeProperty();

    property->addRequiredParameter("include"_qs);
    property->addParameters(yaml);

    return property;
}
