// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include "Property.h"

namespace Fortuna {

class DiscreteDistributionProperty : public Property
{
    Q_OBJECT

public:
    DiscreteDistributionProperty(QObject* parent = nullptr);
    ~DiscreteDistributionProperty();

    void generate() override;

    static Property* createDiscreteDistributionProperty(const YAML::Node& yaml);
};

}
