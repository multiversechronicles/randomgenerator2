// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "DirectoryProperty.h"

#include <algorithm>
#include <string>

#include <QDebug>

#include "RandomEngine.h"
#include "GeneratedObject.h"
#include "IncludeProperty.h"
#include "ObjectProperty.h"

using namespace Fortuna;
using namespace std::string_literals;
namespace fs = std::filesystem;

FORTUNA_REGISTER_PROPERTY_TYPE(directory, DirectoryProperty::create)

std::string tolower(const std::string& input)
{
    std::string result;
    result.resize(input.size());
    std::transform(input.begin(), input.end(), result.begin(), [](auto character) {
        return std::tolower(character);
    });
    return result;
}

DirectoryProperty::DirectoryProperty(QObject* parent)
    : Property(parent)
{
}

DirectoryProperty::~DirectoryProperty()
{
}

QVariantList DirectoryProperty::entries() const
{
    return childPropertyValues();
}

QString DirectoryProperty::directory() const
{
    return parameterValue<QString>("directory"_qs).value_or(QString{});
}

void DirectoryProperty::generate()
{
    Property::generate();

    auto directoryParam = parameterValue<QString>("directory"_qs).value_or(QString{});
    if (directoryParam.isEmpty()) {
        throw PropertyException(path(), "'directory' parameter cannot be empty"_qs);
    }

    auto excludeParam = parameterValue<QVariantList>("exclude"_qs).value_or(QVariantList{});
    std::set<std::string> exclude;
    std::transform(excludeParam.begin(), excludeParam.end(), std::inserter(exclude, exclude.begin()), [](const QVariant& entry) {
        return entry.toString().toLower().toStdString();
    });

    auto asObjectParam = parameterValue<bool>("as_object"_qs).value_or(false);

    auto self = findSelf();
    auto directoryPath = self->filePath().replace_filename(directoryParam.toStdString());

    if (!fs::exists(directoryPath)) {
        throw PropertyException(path(), "Could not find directory "_qs + QString::fromStdString(directoryPath.string()));
    }

    std::vector<fs::path> entries;
    for (const auto& entry : fs::directory_iterator(directoryPath, fs::directory_options::skip_permission_denied)) {
        auto extension = entry.path().extension();
        if (extension != ".yml"s && extension != ".yaml"s) {
            continue;
        }

        entries.push_back(entry.path());
    }

    std::stable_sort(entries.begin(), entries.end(), [](const auto& first, const auto& second) {
        return tolower(first.filename().string()) < tolower(second.filename().string());
    });

    for (const auto& entry : entries) {
        auto lowerFilename = tolower(entry.filename().string());
        if (exclude.find(lowerFilename) != exclude.end()) {
            continue;
        }

        auto relativePath = fs::relative(entry, self->filePath().remove_filename());

        YAML::Node node;
        if (asObjectParam) {
            node["type"s] = "object"s;
            node["object"s] = relativePath.string();
        } else {
            node["type"s] = "include"s;
            node["include"s] = relativePath.string();
        }

        auto property = generateProperty(entry.filename().string(), node, this);
        addChildProperty(property);
    }

    setValue(childPropertyValues());
}

Property* DirectoryProperty::create(const YAML::Node& yaml)
{
    auto property = new DirectoryProperty();

    property->addRequiredParameter("directory"_qs);
    property->addParameters(yaml);

    return property;
}
