// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include "Property.h"

namespace Fortuna {

class SelectProperty : public Property
{
    Q_OBJECT

public:
    SelectProperty(QObject* parent = nullptr);
    ~SelectProperty() override;

    virtual void generate() override;

    static Property* createSelectProperty(const YAML::Node& yaml);

private:
};

}
