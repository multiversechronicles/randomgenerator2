// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include "Property.h"

namespace Fortuna {

class IncludeProperty : public Property
{
    Q_OBJECT
    Q_PROPERTY(Fortuna::Property* includedProperty READ includedProperty CONSTANT)

public:
    IncludeProperty(QObject* parent = nullptr);
    ~IncludeProperty() override;

    Property* includedProperty() const;

    virtual void generate() override;

    static Property* createIncludeProperty(const YAML::Node& yaml);

private:
    Property* m_includedProperty = nullptr;
};

}
