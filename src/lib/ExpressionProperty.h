// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include "Property.h"

namespace Fortuna {

class ExpressionProperty : public Property
{
    Q_OBJECT
    Q_PROPERTY(QString expression READ expression CONSTANT)

public:
    ExpressionProperty(QObject* parent = nullptr);
    ~ExpressionProperty() override;

    QString expression() const;

    void generate() override;

    static Property* createExpressionProperty(const YAML::Node& yaml);

private:
    Property* findFileRoot();

    class Private;
    const std::unique_ptr<Private> d;
};

}
