// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "GeneratedObject.h"

#include <yaml-cpp/yaml.h>
#include <QDebug>

#include "Property.h"
#include "Exceptions.h"

using namespace Fortuna;

namespace fs = std::filesystem;

QString indent(int indentation)
{
    return QString(indentation * 4, QLatin1Char(' '));
}

class FORTUNA_NO_EXPORT GeneratedObject::Private
{
public:
    QVector<Property*> properties;
    QVector<GeneratorException> exceptions;
    QString fileName;

    // Used to keep track of which objects have already been output during
    // toString() calls. This is done to prevent circular references causing
    // infinite recursion.
    static QVector<const GeneratedObject*> printedObjects;
};

QVector<const GeneratedObject*> GeneratedObject::Private::printedObjects;

GeneratedObject::GeneratedObject(const QString& fileName, QObject* parent)
    : QQmlPropertyMap(this, parent)
    , d(std::make_unique<Private>())
{
    d->fileName = fileName;
}

GeneratedObject::~GeneratedObject()
{
    Private::printedObjects.removeOne(this);
}

QQmlListProperty<Property> GeneratedObject::propertiesList()
{
    return QQmlListProperty<Property>(this, this, &GeneratedObject::propertyCount, &GeneratedObject::propertyAt);
}

QVector<Property*> GeneratedObject::properties() const
{
    return d->properties;
}

void GeneratedObject::setPropertyValue(const QString& propertyName, const QVariant& value)
{
    for (auto property : std::as_const(d->properties)) {
        if (property->name() == propertyName) {
            property->setValue(value);
            insert(property->name(), value);
        }
    }
}

bool GeneratedObject::hasProperty(const QString& propertyName)
{
    auto itr = std::find_if(d->properties.cbegin(), d->properties.cend(), [propertyName](Property* property) {
        return property->name() == propertyName;
    });
    return itr != d->properties.cend();
}

QString GeneratedObject::fileName() const
{
    return d->fileName;
}

fs::path GeneratedObject::filePath() const
{
    return fs::path(d->fileName.toStdString());
}

void GeneratedObject::generate()
{
    QVector<GeneratorException> exceptions;

    for (auto property : std::as_const(d->properties)) {
        if (!property->hasValue()) {
            try {
                property->generate();
            } catch(const GeneratorException& e) {
                exceptions.push_back(e);
            }
        }
        insert(property->name(), property->value());
    }

    d->exceptions = exceptions;
}

QVector<GeneratorException> GeneratedObject::exceptions() const
{
    auto result = d->exceptions;

    for (auto property : std::as_const(d->properties)) {
        result.append(property->exceptions());
    }

    return result;
}

QString GeneratedObject::toString(int indentation) const
{
    QString result;

    Private::printedObjects.append(this);
    result += indent(indentation) + d->fileName.mid(d->fileName.lastIndexOf(QLatin1Char('/')) + 1) + QLatin1String(":\n");

    for (auto property : std::as_const(d->properties)) {
        if (property->value().canConvert<GeneratedObject*>()) {
            auto object = property->value().value<GeneratedObject*>();
            if (Private::printedObjects.contains(object)) {
                continue;
            } else {
                result += object->toString(indentation + 1);
            }
        } else {
            result += property->toString(indentation + 1);
        }
    }

    return result;
}

GeneratedObject* GeneratedObject::fromFile(const std::filesystem::path& path)
{
    YAML::Node yaml;

    auto absolute_path = path;
    if (!absolute_path.is_absolute()) {
        absolute_path = std::filesystem::absolute(path);
    }

    if (!fs::exists(absolute_path)) {
        throw GeneratorException("Could not find file "_qs + QString::fromStdString(absolute_path.string()));
    }

    try {
        absolute_path = std::filesystem::canonical(absolute_path);
    } catch (std::filesystem::filesystem_error& e) {
        throw GeneratorException(QString::fromUtf8(e.what()));
    }

    try {
        yaml = YAML::LoadFile(absolute_path.string());
    } catch (YAML::BadFile& e) {
        throw GeneratorException("Could not find file "_qs + QString::fromStdString(absolute_path.string()));
    } catch (YAML::ParserException& e) {
        throw GeneratorException("Error parsing file %1: %2"_qs
                                    .arg(QString::fromStdString(absolute_path.string()))
                                    .arg(QString::fromUtf8(e.what())));
    } catch (YAML::Exception& e) {
        throw GeneratorException("YAML error in file %1: %2"_qs
                                    .arg(QString::fromStdString(absolute_path.string()))
                                    .arg(QString::fromUtf8(e.what())));
    }

    auto object = new GeneratedObject(QString::fromStdString(absolute_path.string()));

    for (auto itr = yaml.begin(); itr != yaml.end(); ++itr) {
        auto key = itr->first.as<std::string>();
        if (key.empty() || key.front() == '.') {
            continue;
        }

        auto property = Property::fromYAML(itr->first.as<std::string>(), itr->second);
        property->setParent(object);
        object->d->properties << property;
    }

    return object;
}

int GeneratedObject::propertyCount(QQmlListProperty<Property>* list)
{
    return reinterpret_cast<GeneratedObject*>(list->data)->d->properties.count();
}

Property* GeneratedObject::propertyAt(QQmlListProperty<Property>* list, int index)
{
    return reinterpret_cast<GeneratedObject*>(list->data)->d->properties.at(index);
}
