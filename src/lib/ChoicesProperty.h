// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include "Property.h"

namespace Fortuna {

class ChoicesProperty : public Property
{
    Q_OBJECT
    Q_PROPERTY(QVariantList choices READ choices CONSTANT)
    Q_PROPERTY(Fortuna::Property* chosen READ chosen CONSTANT)

public:
    ChoicesProperty(QObject* parent = nullptr);
    ~ChoicesProperty() override;

    QVariantList choices() const;

    virtual void generate() override;

    Property* chosen() const;

    static Property* create(const YAML::Node& yaml);

private:
    YAML::Node m_choices;
    std::optional<YAML::Node> m_choice;
    Property* m_choiceProperty = nullptr;
    Property* m_chosenProperty = nullptr;
};

}
