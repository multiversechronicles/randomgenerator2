// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "RandomEngine.h"

#include <QVariant>

using namespace Fortuna;

class RandomEngine::Private
{
public:
    EngineType randomEngine;
    EngineType::result_type seed = EngineType::default_seed;
};

RandomEngine::RandomEngine()
    : d(std::make_unique<Private>())
{
    reset(ResetSeed);
}

RandomEngine::~RandomEngine() = default;

RandomEngine::EngineType& RandomEngine::toStdRandomEngine()
{
    return d->randomEngine;
}

uint RandomEngine::seed() const
{
    return uint(d->seed);
}

void RandomEngine::setSeed(uint newSeed)
{
    if (newSeed == uint(d->seed)) {
        return;
    }

    d->seed = newSeed;
    d->randomEngine.seed(newSeed);
    Q_EMIT seedChanged();
}

void RandomEngine::reset(ResetFlags flags)
{
    if (flags & ResetFlag::ResetSeed) {
        std::random_device device;
        setSeed(device());
    }

    if (flags & ResetFlag::ResetState) {
        d->randomEngine = std::mt19937_64(d->seed);
    }
}

qreal RandomEngine::value(qreal min, qreal max)
{
    return realValue(min, max);
}

int RandomEngine::intValue(int min, int max)
{
    std::uniform_int_distribution<int> distribution{min, max};
    return distribution(d->randomEngine);
}

qreal RandomEngine::realValue(qreal min, qreal max)
{
    std::uniform_real_distribution<qreal> distribution{min, max};
    return distribution(d->randomEngine);
}

int RandomEngine::roll(int count, int sides, int modifier)
{
    std::uniform_int_distribution<int> distribution{1, sides};

    auto result = 0;
    while (count-- > 0) {
        result += distribution(d->randomEngine);
    }
    result += modifier;
    return result;
}

QVariant RandomEngine::choice(const QVariantList& list)
{
    return list.value(intValue(0, list.size() - 1));
}

std::shared_ptr<RandomEngine> RandomEngine::instance()
{
    static auto engine = std::make_shared<RandomEngine>();
    return engine;
}
