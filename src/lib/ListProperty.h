// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include "Property.h"

namespace Fortuna {

class ListProperty : public Property
{
    Q_OBJECT
    Q_PROPERTY(int count READ count CONSTANT)

public:
    ListProperty(QObject* parent = nullptr);
    ~ListProperty() override;

//     QVector<GeneratedObject*> objects() const;
    int count() const;

    virtual void generate() override;

    QString toString(int indentation = 0) const override;

    static Property* createListProperty(const YAML::Node& yaml);

private:
    YAML::Node m_template;
    QVector<Property*> m_entries;
};

}
