// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
// 
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include <memory>
#include <random>

#include <QObject>

#include "fortuna_export.h"

namespace Fortuna {

class FORTUNA_EXPORT RandomEngine : public QObject
{
    Q_OBJECT
public:
    enum ResetFlag {
        ResetState = 0x1,
        ResetSeed = 0x2,
    };
    Q_DECLARE_FLAGS(ResetFlags, ResetFlag)
    Q_FLAGS(ResetFlags)

    using EngineType = std::mt19937_64;

    RandomEngine();
    ~RandomEngine() override;

    EngineType& toStdRandomEngine();

    Q_PROPERTY(uint seed READ seed WRITE setSeed NOTIFY seedChanged)
    uint seed() const;
    void setSeed(uint seed);
    Q_SIGNAL void seedChanged();

    Q_INVOKABLE void reset(ResetFlags flags);

    Q_INVOKABLE qreal value(qreal min, qreal max);
    Q_INVOKABLE int intValue(int min, int max);
    Q_INVOKABLE qreal realValue(qreal min, qreal max);
    Q_INVOKABLE int roll(int count, int sides, int modifier);
    Q_INVOKABLE QVariant choice(const QVariantList& list);

    static std::shared_ptr<RandomEngine> instance();

private:
    class Private;
    const std::unique_ptr<Private> d;
};

}

Q_DECLARE_OPERATORS_FOR_FLAGS(Fortuna::RandomEngine::ResetFlags)
