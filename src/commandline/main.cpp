#include <memory>
#include <string>
#include <iostream>

#include <QCommandLineParser>
#include <QCoreApplication>

#include "Generator.h"
#include "GeneratedObject.h"
#include "Property.h"

int main(int argc, char** argv)
{
    QCoreApplication app(argc, argv);

    QCommandLineParser parser;
    parser.addOption({QStringLiteral("list-types"), QStringLiteral("List all property types.")});
    parser.addOption({{QStringLiteral("p"), QStringLiteral("property")}, QStringLiteral("Only display property <property>"), QStringLiteral("property")});
    parser.addPositionalArgument(QStringLiteral("file"), QStringLiteral("A YAML file to load."));
    parser.addHelpOption();
    parser.process(app);

    if (parser.isSet(QStringLiteral("list-types"))) {
        std::cout << qPrintable(Fortuna::Property::listTypes()) << std::endl;
        return 0;
    }

    if (parser.positionalArguments().count() != 1) {
        parser.showHelp();
        return 1;
    }

    Fortuna::Generator generator;
    generator.setFile(QUrl::fromLocalFile(parser.positionalArguments().first()));
    if (parser.isSet("property"_qs)) {
        generator.setPropertyName(parser.value("property"_qs));
    }

    generator.generate();

    std::cout << qPrintable(generator.generatedText()) << std::endl;

    if (!generator.exceptionText().isEmpty()) {
        std::cout << "\nExceptions were encountered generating the object:\n";
        std::cout << qPrintable(generator.exceptionText()) << std::endl;
    }

}

