add_executable(fortunacmd ${commandline_SRCS})

target_sources(fortunacmd PRIVATE main.cpp)

target_link_libraries(fortunacmd Fortuna)

install(TARGETS fortunacmd DESTINATION ${BINARY_INSTALL_DIR})
