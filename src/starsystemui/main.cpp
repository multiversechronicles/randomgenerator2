
#include <QCommandLineParser>
#include <QDebug>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QApplication>

#include "GeneratedObject.h"
#include "Property.h"

int main(int argc, char** argv)
{
    auto app = std::make_unique<QApplication>(argc, argv);

    auto parser = std::make_unique<QCommandLineParser>();
    parser->addPositionalArgument(QStringLiteral("file"), QStringLiteral("A YAML file to load."));
    parser->addHelpOption();
    parser->process(*app);

    if (parser->positionalArguments().count() != 1) {
        parser->showHelp();
        return 1;
    }

    auto url = QUrl::fromLocalFile(parser->positionalArguments().at(0));

    auto engine = std::make_unique<QQmlApplicationEngine>();
    engine->rootContext()->setContextProperty("_commandline_file"_qs, url);
    engine->load("qrc:/main.qml"_qs);

    return app->exec();
}


