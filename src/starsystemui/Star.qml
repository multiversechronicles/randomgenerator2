import QtQuick 2.12
import QtQuick.Controls 2.12
import Qt.labs.qmlmodels 1.0

import QtGraphicalEffects 1.0
import QtQuick.Particles 2.12

Item {
    id: root

    property QtObject star
//     radius: root.star.distance

    property Component starDelegate

    Circle {
        anchors.centerIn: parent
        radius: root.star.outerLimit * ApplicationWindow.window.auScalingFactor * 2
        color: "transparent"
        opacity: 0.5
        border.width: 3
        border.color: "green"
    }

    Circle {
        anchors.centerIn: parent
        radius: root.star.snowLine * ApplicationWindow.window.auScalingFactor * 2
        color: "transparent"
        opacity: 0.5
        border.width: 3
        border.color: "blue"
    }

    Circle {
        anchors.centerIn: parent
        radius: root.star.innerLimit * ApplicationWindow.window.auScalingFactor * 2
        color: "transparent"
        opacity: 0.5
        border.width: 3
        border.color: "green"
    }

    Circle {
        anchors.centerIn: parent
        radius: root.star.innerForbiddenZone * ApplicationWindow.window.auScalingFactor * 2
        color: "transparent"
        opacity: 0.5
        border.width: 3
        border.color: "red"
    }

    Circle {
        anchors.centerIn: parent
        radius: root.star.outerForbiddenZone * ApplicationWindow.window.auScalingFactor * 2
        color: "transparent"
        opacity: 0.5
        border.width: 3
        border.color: "red"
    }

    Repeater {
        model: star.companions

        delegate: root.starDelegate
    }

    Repeater {
        model: star.planets

        Loader {
            anchors.centerIn: parent
            property var __loader_planet: modelData

            sourceComponent: {
                if (modelData == null) {
                    return null
                }

                if (modelData.type == "Asteroid Belt") {
                    return asteroidBelt
                } else if (modelData.type == "Planet") {
                    switch(modelData.size) {
                        case "Tiny": return tinyPlanet
                        case "Small": return smallPlanet
                        case "Standard": return standardPlanet
                        case "Large": return largePlanet
                    }
                } else if (modelData.type == "Gas Giant") {
                    switch(modelData.size) {
                        case "Small": return smallGasGiant
                        case "Medium": return mediumGasGiant
                        case "Large": return largeGasGiant
                    }
                }
            }

            Component {
                id: asteroidBelt

                Asteroids { planet: __loader_planet }
            }

            Component {
                id: tinyPlanet

                Planet {
                    size: 0.01
                    color: "lightGrey"
                }
            }

            Component {
                id: smallPlanet

                Planet {
                    size: 0.015
                    color: "grey"
                }
            }

            Component {
                id: standardPlanet

                Planet {
                    width: 0.02
                    color: "darkGrey"
                }
            }

            Component {
                id: largePlanet

                Planet {
                    size: 0.025
                    color: "darkGrey"
                }
            }

            Component {
                id: smallGasGiant

                Planet {
                    size: 0.04
                    color: "orange"
                }
            }

            Component {
                id: mediumGasGiant

                Planet {
                    size: 0.05
                    color: "cyan"
                }
            }

            Component {
                id: largeGasGiant

                Planet {
                    size: 0.06
                    color: "blue"
                }
            }
        }
    }

    ParticleSystem {
        id: particles

        Emitter {
            emitRate: 100
            lifeSpan: 1000
            lifeSpanVariation: 500
            size: root.star.radius * 10 * ApplicationWindow.window.auScalingFactor
        }

        ImageParticle {
            width: 32
            height: 32
            rotationVariation: 180
            rotationVelocityVariation: 10
            source: "qrc:/images/star.png"
            color: {
                var r = 0
                var g = 0
                var b = 0
                var temp = (root.star.temperature / 100) - 20;

                if (temp < 66) {
                    r = 1.0
                    g = saturate(0.39008157876901960784 * Math.log(temp) - 0.63184144378862745098);
                } else {
                    r = saturate(1.29293618606274509804 * Math.pow(temp - 60, -0.1332047592));
                    g = saturate(1.12989086089529411765 * Math.pow(temp - 60, -0.0755148492));
                }

                if (temp >= 66.0) {
                    b = 1.0;
                } else if(temp <= 19.0) {
                    b = 0.01;
                } else {
                    b = saturate(0.54320678911019607843 * Math.log(temp - 10.0) - 1.19625408914);
                }

                return Qt.rgba(r, g, b, 0.5);
            }

            function saturate(value) {
                return Math.min(Math.max(value, 0.0), 1.0)
            }
        }

        Connections {
            target: ApplicationWindow.window
            onAuScalingFactorChanged: particles.reset()
        }
    }

    Circle {
        id: highlight

        anchors.centerIn: parent

        visible: mouse.containsMouse

        radius: {
            let result = root.star.radius * 10 * ApplicationWindow.window.auScalingFactor
            return Math.max(result, 10)
        }

        color: "transparent"
        border.width: 3
        border.color: "white"
    }

    MouseArea {
        id: mouse
        anchors.fill: highlight
        hoverEnabled: true
        onClicked: {
            details.x = mouse.x
            details.y = mouse.y
            details.open()
        }
    }

    StarDetails {
        id: details

        object: root.star
    }
}
