import QtQuick 2.12
import QtQuick.Controls 2.12

Orbit {
    id: root

    property QtObject planet: __loader_planet
    radius: planet != null ? planet.distance : 0

    property real size
    property color color: "grey"

    Rectangle {
        width: root.size * ApplicationWindow.window.auScalingFactor
        height: width
        radius: width / 2
        color: root.color
    }
}

