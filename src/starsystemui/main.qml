import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import Fortuna 1.0 as Fortuna

ApplicationWindow {
    id: window

    visible: true
    width: 1024
    height: 768

    header: RowLayout {
        Button { text: "Generate"; onClicked: generator.generate() }
        Button { text: "Zoom In"; onClicked: {
            view.width *= 2
            flickable.contentX = view.width / 2 - flickable.width / 2
            flickable.contentY = view.height / 2 - flickable.height / 2
        } }
        Button { text: "Zoom Out"; onClicked: {
            view.width /= 2
            flickable.contentX = view.width / 2 - flickable.width / 2
            flickable.contentY = view.height / 2 - flickable.height / 2
        } }
    }

    property alias starSystem: generator.object

    property real auScalingFactor: (view.width / 2) / starSystem.primary.outerLimit

    Fortuna.Generator {
        id: generator

        file: _commandline_file

        onExceptionTextChanged: print(exceptionText)
        Component.onCompleted: generate()
    }

    //Pane {
        //id: details

        //anchors {
            //top: parent.top
            //left: parent.left
            //bottom: parent.bottom
        //}
        //width: 350

        //GridLayout {
            //anchors.fill: parent
            //anchors.margins: 10

            //columns: 2

            //Label { Layout.columnSpan: 2; text: starSystem.name; font.pointSize: 20; }

            //Label { text: "Age"; }
            //Label { text: starSystem.age.toFixed(3) + " Ga"; }

            //Label { Layout.columnSpan: 2; text: starSystem.primary.name; font.pointSize: 16 }

            //Label { text: "Class" }
            //Label { text: starSystem.primary.ageClass }

            //Label { text: "Spectral Type"; }
            //Label { text: starSystem.primary.type.name; }

            //Label { text: "Mass" }
            //Label { text: starSystem.primary.mass.toFixed(3) + " Ms"; }

            //Label { text: "Temperature";  }
            //Label { text: starSystem.primary.temperature.toFixed(3) + " K" }

            //Label { text: "Luminosity" }
            //Label { text: starSystem.primary.luminosity.toFixed(3) + " Ls" }

            //Label { text: "Radius" }
            //Label { text: starSystem.primary.radius.toFixed(3) + " AU" }

            //Label { text: "Inner Limit"}
            //Label { text: starSystem.primary.innerLimit.toFixed(3) + " AU" }

            //Label { text: "Outer Limit"}
            //Label { text: starSystem.primary.outerLimit.toFixed(3) + " AU" }

            //Label { text: "Snow Line"}
            //Label { text: starSystem.primary.snowLine.toFixed(3) + " AU" }

            //Label { text: "Inner Forbidden Zone" }
            //Label { text: starSystem.primary.innerForbiddenZone.toFixed(3) + " AU" }

            //Label { text: "Outer Forbidden Zone" }
            //Label { text: starSystem.primary.outerForbiddenZone.toFixed(3) + " AU" }

            //Label { text: "Companions"}
            //Label { text: starSystem.primary.companions.length }

            //Label { text: "Planets" }
            //Label { text: starSystem.primary.planets.reduce((acc, cur) => cur != null ? acc + 1 : acc, 0) }

            //Item { Layout.fillWidth: true; Layout.fillHeight: true }
        //}
    //}

    Rectangle {
        anchors {
            left: details.right
            top: parent.top
            bottom: parent.bottom
            right: parent.right
        }
        color: "black"

        Flickable {
            id: flickable

            anchors.fill: parent
            contentWidth: view.width
            contentHeight: view.height
            contentX: view.width / 2 - flickable.width / 2
            contentY: view.height / 2 - flickable.height / 2
            boundsBehavior: Flickable.StopAtBounds
            clip: true

            Item {
                id: view

                width: Math.min(window.width, window.height)
                height: width

                Star {
                    anchors.centerIn: parent
                    star: generator.object ? generator.object.primary : null

                    starDelegate: Orbit {
                        anchors.centerIn: parent;
                        radius: modelData.distance
                        Star { star: modelData }
                    }
                }
            }
        }
    }
}
