import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Popup {
    id: root

    property var object

    modal: true

    Overlay.modal: Item { }

    background: Rectangle {
        color: "white"
        radius: 5
    }

    contentItem: GridLayout {
        id: layout

        columns: 2

        Label { Layout.columnSpan: 2; text: root.object ? root.object.name : ""; font.pointSize: 16; }

        //Label { text: "Age"; }
        //Label { text: starSystem.age.toFixed(3) + " Ga"; }

        //Label { Layout.columnSpan: 2; text: starSystem.primary.name; font.pointSize: 16 }

        Label { text: "Class" }
        Label { text: root.object ? root.object.ageClass : "" }

        Label { text: "Spectral Type"; }
        Label { text: root.object ? root.object.type.name : "" }

        Label { text: "Mass" }
        Label { text: root.object ? root.object.mass.toFixed(3) + " Ms" : "" }

        Label { text: "Temperature";  }
        Label { text: root.object ? root.object.temperature.toFixed(3) + " K" : "" }

        Label { text: "Luminosity" }
        Label { text: root.object ? root.object.luminosity.toFixed(3) + " Ls" : "" }

        Label { text: "Radius" }
        Label { text: root.object ? root.object.radius.toFixed(3) + " AU" : "" }

        //Label { text: "Inner Limit"}
        //Label { text: starSystem.primary.innerLimit.toFixed(3) + " AU" }

        //Label { text: "Outer Limit"}
        //Label { text: starSystem.primary.outerLimit.toFixed(3) + " AU" }

        //Label { text: "Snow Line"}
        //Label { text: starSystem.primary.snowLine.toFixed(3) + " AU" }

        //Label { text: "Inner Forbidden Zone" }
        //Label { text: starSystem.primary.innerForbiddenZone.toFixed(3) + " AU" }

        //Label { text: "Outer Forbidden Zone" }
        //Label { text: starSystem.primary.outerForbiddenZone.toFixed(3) + " AU" }

        //Label { text: "Companions"}
        //Label { text: starSystem.primary.companions.length }

        //Label { text: "Planets" }
        //Label { text: starSystem.primary.planets.reduce((acc, cur) => cur != null ? acc + 1 : acc, 0) }

        Item { Layout.fillWidth: true; Layout.fillHeight: true }
    }
}
