import QtQuick 2.12
import QtQuick.Controls 2.12

Item {
    id: root

    property real radius: 5.0
    default property alias orbitItems: orbitObject.data

    Rectangle {
        anchors.centerIn: parent

        width: root.radius * ApplicationWindow.window.auScalingFactor * 2
        height: width
        radius: width / 2

        color: "transparent"
        border.width: 2
        border.color: Qt.rgba(1.0, 1.0, 1.0, 0.5)

        NumberAnimation on rotation { duration: 120000; from: Math.random() * 360; to: from + 360; running: true; loops: Animation.Infinite }

        Item {
            id: orbitObject
            anchors.verticalCenter: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            width: childrenRect.width
            height: childrenRect.height
            z: 99
        }
    }
}


