import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import Qt.labs.platform 1.0 as Platform

import Fortuna 1.0 as Fortuna

ApplicationWindow {
    id: window

    width: 1280
    height: 720
    visible: true

    function resetEngine() {
        let flags = Fortuna.RandomEngine.ResetState

        if (randomSeedBox.checked) {
            flags |= Fortuna.RandomEngine.ResetSeed
        }

        Fortuna.RandomEngine.reset(flags)
    }

    header: ToolBar {
        RowLayout {
            ToolButton {
                text: "Open"
                icon.name: "document-open"
                onClicked: openFile.open()
            }

            ToolButton {
                text: "Save"
                icon.name: "document-save"
            }

            ToolButton {
                text: "Generate"
                icon.name: "roll"

                onClicked: {
                    window.resetEngine()
                    generator.generate()
                }
            }

            ToolButton {
                text: "Properties"
                icon.name: "help-hint"

                onClicked: help.open()
            }

            ToolButton {
                id: markdownButton

                text: "Markdown"
                icon.name: ""

                checkable: true
            }
        }
    }

    SplitView {
        anchors.fill: parent
        anchors.margins: 5

        ScrollView {
            SplitView.preferredWidth: 0 // window.width * 0.5
            height: parent.height

            TextArea {

            }
        }

        ColumnLayout {
            //SplitView.preferredWidth: window.width * 0.5
            height: parent.height

            RowLayout {
                TextField {
                    id: propertyFilter

                    Layout.fillWidth: true
                    Layout.preferredWidth: 2

                    placeholderText: "Property..."

                    onTextEdited: {
                        generator.propertyName = text
                    }
                }

                Label { text: "Seed:" }

                CheckBox {
                    id: randomSeedBox;
                    text: "Random"
                    checked: true
                }

                TextField {
                    Layout.fillWidth: true;
                    Layout.preferredWidth: 1
                    enabled: !randomSeedBox.checked
                    text: Fortuna.RandomEngine.seed
                    onTextEdited: {
                        Fortuna.RandomEngine.seed = parseInt(text)
                        Fortuna.RandomEngine.reset(Fortuna.RandomEngine.ResetState)
                        generator.generate()
                    }
                }
            }

            ScrollView {
                Layout.fillWidth: true
                Layout.fillHeight: true

                TextArea {
                    readOnly: true

                    textFormat: markdownButton.checked ? TextArea.MarkdownText : TextArea.PlainText

                    text: generator.generatedText
                }
            }

            ScrollView {
                Layout.fillWidth: true
                Layout.fillHeight: generator.hasExceptions

                TextArea {
                    readOnly: true

                    text: generator.exceptionText ? generator.exceptionText : "No errors encountered."
                }
            }
        }
    }

    Platform.FileDialog {
        id: openFile

        onAccepted: {
            window.resetEngine()
            generator.file = file
            generator.generate()
        }
    }

    Platform.FileDialog {
        id: saveFile
    }

    Dialog {
        id: help

        x: window.width / 2 - width / 2
        y: 0
        width: 800
        height: 600

        modal: true
        standardButtons: Dialog.Close
        title: "Properties"

        padding: 5

        ScrollView {
            anchors.fill: parent

            TextArea {
                text: generator.propertyText()

                readOnly: true

                textFormat: TextArea.MarkdownText
            }
        }
    }

    Fortuna.Generator {
        id: generator
    }
}
