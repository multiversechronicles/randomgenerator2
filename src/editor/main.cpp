#include <memory>
#include <string>
#include <iostream>

#include <QCommandLineParser>
#include <QDebug>
#include <QApplication>
#include <QQmlApplicationEngine>

#include "Property.h"
#include "Generator.h"
#include "GeneratedObject.h"

int main(int argc, char** argv)
{
    QApplication app{argc, argv};
    app.setOrganizationDomain("org.multiversechronicles"_qs);
    app.setApplicationDisplayName("Fortuna Editor"_qs);

    QCommandLineParser parser;
    parser.addPositionalArgument("file"_qs, "A YAML file to load."_qs);
    parser.addHelpOption();
    parser.process(app);

    QQmlApplicationEngine engine;
    engine.load("qrc:/main.qml"_qs);

    return app.exec();
}
