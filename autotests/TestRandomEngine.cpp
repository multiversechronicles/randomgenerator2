/*
 * SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <QtTest>

#include "RandomEngine.h"

class TestRandomEngine : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testIntValue()
    {
        auto engine = Fortuna::RandomEngine::instance();

        auto int1 = engine->intValue(0, 100);
        auto int2 = engine->intValue(0, 100);

        QVERIFY(int1 >= 0);
        QVERIFY(int1 <= 100);

        QVERIFY(int2 >= 0);
        QVERIFY(int2 <= 100);

        QVERIFY(int1 != int2);
    }

    void testReset()
    {
        auto engine = Fortuna::RandomEngine::instance();

        engine->setSeed(1234567890);

        auto firstInt = engine->intValue(0, 100);

        QVERIFY(engine->intValue(0, 100) != firstInt);

        engine->reset(Fortuna::RandomEngine::ResetState);

        QCOMPARE(engine->intValue(0, 100), firstInt);

        engine->reset(Fortuna::RandomEngine::ResetState | Fortuna::RandomEngine::ResetSeed);

        QVERIFY(engine->intValue(0, 100) != firstInt);
    }
};

QTEST_MAIN(TestRandomEngine)

#include "TestRandomEngine.moc"
